## aosp_raphael-user 12 SP1A.210812.016 eng.abhish.20211005.161234 test-keys
- Manufacturer: xiaomi
- Platform: 
- Codename: raphael
- Brand: Xiaomi
- Flavor: aosp_raphael-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.abhish.20211005.161234
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: 440
- Fingerprint: Xiaomi/raphaelin/raphaelin:9/PKQ1.181121.001/V10.3.3.0.PFKINXM:user/release-keys
- OTA version: 
- Branch: aosp_raphael-user-12-SP1A.210812.016-eng.abhish.20211005.161234-test-keys
- Repo: xiaomi_raphael_dump_4777


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
